<?php

namespace AppBundle\Notification\Gateways;


interface EmailGateway
{
    public function sendEmail($to,$message,$subject,$params);
}