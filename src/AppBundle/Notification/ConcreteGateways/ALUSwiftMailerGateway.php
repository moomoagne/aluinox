<?php

namespace AppBundle\Notification\ConcreteGateways;

use AppBundle\Notification\Gateways\EmailGateway;
use Symfony\Component\Templating\EngineInterface;


class ALUSwiftMailerGateway implements EmailGateway
{

    protected $mailer;
    protected $templating;
    private $from = "no-reply@aluinox.com";
    private $name = "no-reply@aluinox.com";
    //private $name = "AluInox";

    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendEmail($to, $message, $subject, $params = array())
    {
        $this->sendMail($to, $subject, $message, $params);
    }

    protected function sendMail($to, $subject, $body, $params = array())
    {
        $mail = \Swift_Message::newInstance();

        $mail->setFrom($this->from, $this->name)
            ->setTo($to)
            ->setSubject($subject)
            ->setBody($this->renderEmailBody($body, $params))
            ->setContentType("text/html");
        $this->mailer->send($mail);
    }

    protected function renderEmailBody($template, $params = array())
    {
        return $this->templating->render($template, $params);
    }

}