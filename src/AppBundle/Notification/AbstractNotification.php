<?php


namespace AppBundle\Notification;


use AppBundle\Model\NotificationObject;
use AppBundle\Notification\Gateways\EmailGateway;

abstract class AbstractNotification
{

    protected $emailGateway;

    /**
     * AbstractNotification constructor.
     * @param EmailGateway|null $emailGateway
     */


    public function __construct (EmailGateway $emailGateway = null)
    {
        $this->emailGateway = $emailGateway;
    }

    /**
     * @param NotificationObject $notificatiown
     * @return mixed
     */
    public abstract function notify(NotificationObject $notification);

    public function sendEmail($to, $message, $subject,$params)
    {
        $this->emailGateway->sendEmail($to,$message,$subject,$params);
    }

}