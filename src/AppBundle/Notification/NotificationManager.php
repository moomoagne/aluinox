<?php


namespace AppBundle\Notification;


use AppBundle\Model\NotificationObject;
use AppBundle\Entity\User;

class NotificationManager extends AbstractNotification
{
    const EMAIL_ONLY = 1;


    public function notify(NotificationObject $notification)
    {
        $notification->getType();
        $this->sendEmail(
            $notification->getToEmail(),
            $notification->getMailBody(),
            $notification->getSubject(),
            $notification->getParams());
    }

}