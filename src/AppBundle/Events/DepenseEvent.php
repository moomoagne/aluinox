<?php
/**
 * Created by PhpStorm.
 * User: mamadou
 * Date: 11/23/17
 * Time: 12:51 PM
 */

namespace AppBundle\Events;

use AppBundle\Entity\Depense;
use Symfony\Component\EventDispatcher\Event;

class DepenseEvent extends Event
{
    protected $depense;

    public function __construct(Depense $depense)
    {
        $this->depense = $depense;
    }

    /**
     * @return Depense
     */
    public function getDepense ()
    {
        return $this->depense;
    }



}