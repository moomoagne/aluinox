<?php

namespace AppBundle\Events;


class NotificationEvent
{
    /**
     * NEW_DEPENSE
     */
    const NEW_DEPENSE = 'alu.depense.new';

    const ACTIVE_CHANTIER = 'alu.active.chantier';

    const NEW_CHANTIER = 'alu.chantier.new';
}