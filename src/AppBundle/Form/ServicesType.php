<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServicesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('natureCeintrage')
            ->add('client')
            ->add('telephone')
            ->add('montant')
            ->add('montantAvance')
            ->add('montantRestant',HiddenType::class)
            ->add('dateCreation',TextType::class)
            ->get('dateCreation')->addModelTransformer(new CallbackTransformer(
                    function($datToString){
                        if($datToString instanceof \DateTime){
                            return $datToString->format('d-m-Y');
                        }
                        return "";
                    },
                    function($stringAsDate){
                        if(is_string($stringAsDate)){
                            return new \DateTime($stringAsDate);
                        }
                        return new \DateTime("now");
                    }
                )
            )
        ;
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Services'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_services';
    }


}
