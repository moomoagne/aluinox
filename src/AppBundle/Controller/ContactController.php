<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use function dump;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Contact controller.
 *
 * @Route("contact")
 */
class ContactController extends Controller
{
    /**
     * Lists all contact entities.
     *
     * @Route("/", name="contact_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contacts = $em->getRepository('AppBundle:Contact')->findAll();

        return $this->render('contact/index.html.twig', array(
            'contacts' => $contacts,
        ));
    }

    /**
     * Creates a new contact entity.
     *
     * @Route("/new", name="contact_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm('AppBundle\Form\ContactType', $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $message = \Swift_Message::newInstance()
                ->setSubject("Contact")
                ->setFrom('no-reply@aluinox.com')
                ->setTo('taboure.agne@gmail.com')
                ->setBody($this->renderView('Emails/contactemail.html.twig',array(
                    'prenom' => $contact->getPrenom(),
                    'nom' => $contact->getNom(),
                    'email' => $contact->getEmail(),
                    'telephone' => $contact->getTelephone(),
                    'message' => $contact->getMessage(),

                )),'text/html');

            $this->get('mailer')->send($message);
            //dump($contact);die();
            $em->persist($contact);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Votre message de contact à bien été envoyer !! ')
            ;
            return $this->redirectToRoute('contact_new');

        }

        return $this->render('contact/new.html.twig', array(
            'contact' => $contact,
            'form' => $form->createView(),
        ));
    }


}

