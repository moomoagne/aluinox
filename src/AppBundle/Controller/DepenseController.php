<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Chantier;
use AppBundle\Entity\Depense;
use AppBundle\Entity\User;
use AppBundle\Events\ChantierEvent;
use AppBundle\Events\DepenseEvent;
use AppBundle\Events\NotificationEvent;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Repository\DepenseRepository;

/**
 * Depense controller.
 *
 * @Route
 */
class DepenseController extends Controller
{
    /**
     * Lists all depense entities.
     *
     * @Route("depense/", name="depense_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $depenses = $em->getRepository('AppBundle:Depense')->findAll();

        return $this->render('depense/index.html.twig', array(
            'depenses' => $depenses,
        ));
    }

    /**
     * Creates a new depense entity
     * @Route("depense/new/{idChantier}", name="depense_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Chantier $idChantier)
    {

        $depense = new Depense();
        $depense->setChantier($idChantier);
        $form = $this->createForm('AppBundle\Form\DepenseType', $depense);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$chantier = $em->getRepository('AppBundle:Chantier')->findBy(array('id'=>$id));

            //$depense->setChantier($chantier);
            $em->persist($depense);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Votre depense '.$depense->getNature().'a bien été creer !! ')
            ;
            $this->dispatch(NotificationEvent::NEW_DEPENSE,new DepenseEvent($depense));
            return $this->redirect($this->generateUrl('depense_chantier_show',array('id'=>$idChantier->getId())));

            //return $this->redirectToRoute('depense_show', array('id' => $depense->getId()));

        }

        return $this->render('depense/new.html.twig', array(
            'id'=>$idChantier->getId(),
            'name'=>$idChantier->getName(),
            'depense' => $depense,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a depense entity.
     *
     * @Route("depense/{id}", name="depense_show")
     * @Method("GET")
     */
    public function showAction(Depense $depense)
    {
        $deleteForm = $this->createDeleteForm($depense);

        return $this->render('depense/show.html.twig', array(
            'depense' => $depense,
            'name' => $depense->getChantier()->getName(),
            'id' => $depense->getChantier()->getId(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a chantier entity.
     *
     * @Route("chantier/{id}/depense", name="depense_chantier_show")
     * @Method("GET")
     */
    public function showDepenseAction(Chantier $id)
    {
        $em = $this->getDoctrine()->getManager();
        $depense = $em->getRepository(Depense::class)
            ->createQueryBuilder('depense')
            ->select('depense')
            ->addSelect('chantier')
            ->leftJoin('depense.chantier', 'chantier')
            ->where('chantier = :chantier_id')
            ->setParameter('chantier_id', $id)
            ->getQuery()
            ->execute();

        $depenseTotalBudget = $em->getRepository(Depense::class)
            ->createQueryBuilder('depense')
            ->addSelect('depense.typeDepense')
            ->addSelect('chantier')
            ->leftJoin('depense.chantier', 'chantier')
            ->where('chantier = :chantier_id')
            ->andwhere('depense.typeDepense = :budget')
            ->addSelect('SUM(depense.montant) AS montantTotal')
            ->setParameters(new ArrayCollection(array(
                new Parameter('chantier_id', $id),
                new Parameter('budget','budget'),
            )))
            ->getQuery()
            ->execute();
        //dump($depenseTotalBudget);die();

        $depenseTotal = $em->getRepository(Depense::class)
            ->createQueryBuilder('depense')
            ->addSelect('chantier')
            ->leftJoin('depense.chantier', 'chantier')
            ->where('chantier = :chantier_id')
            ->addSelect('SUM(depense.montant) AS montantTotal')
            ->setParameter('chantier_id', $id)
            ->getQuery()
            ->execute();
        //dump($depenseTotal);die();
        foreach ($depenseTotalBudget as $d){
            $budget = $id->getBudget() - $d['montantTotal'];
            //echo "".$budget;
        }
        if ($budget <= 0){
            $this->dispatch(NotificationEvent::ACTIVE_CHANTIER, new ChantierEvent($id));
        }
        return $this->render('chantier/showDep.html.twig', array(
            'id' =>$id->getId(),
            'name' => $id->getName(),
            'depenses' =>$depense,
            'montantTotal' =>$depenseTotalBudget,
            'Totals' =>$depenseTotal,
            'budget' => $budget,
        ));
    }



    /**
     * Displays a form to edit an existing depense entity.
     *
     * @Route("depense/{id}/edit", name="depense_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Depense $depense)
    {
        $deleteForm = $this->createDeleteForm($depense);
        $editForm = $this->createForm('AppBundle\Form\DepenseType', $depense);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('depense_edit', array('id' => $depense->getId()));
        }

        return $this->render('depense/edit.html.twig', array(
            'name' => $depense->getChantier()->getName(),
            'id' => $depense->getChantier()->getId(),
            'depense' => $depense,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a depense entity.
     *
     * @Route("depense/{id}", name="depense_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Depense $depense)
    {
        $form = $this->createDeleteForm($depense);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($depense);
            $em->flush();
        }

        return $this->redirectToRoute('depense_index');
    }

    /**
     * Creates a form to delete a depense entity.
     *
     * @param Depense $depense The depense entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Depense $depense)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('depense_delete', array('id' => $depense->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }

    private function dispatch($eventName, Event $event)
    {
        return $this->get('event_dispatcher')->dispatch($eventName,$event);
    }

}
