<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Acompte;
use AppBundle\Entity\Chantier;
use AppBundle\Entity\Depense;
use AppBundle\Events\ChantierEvent;
use AppBundle\Events\NotificationEvent;
use AppBundle\Form\ChantierType;
use function count;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use function dump;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * Chantier controller.
 *
 * @Route("chantier")
 */
class ChantierController extends Controller
{
    /**
     * @Route("/", name="chantier_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $chantiers = $em->getRepository('AppBundle:Chantier')
            ->findBy(array(),array('dateDebut'=>'desc'), 3,0);

        return $this->render('chantier/index.html.twig', array(
            'chantiers' => $chantiers,
        ));
    }

    /**
     * Liste de tous les chantiers
     *
     * @Route("/all", name="all_chantier")
     * @Method("GET")
     */
    public function allChantierIndex ()
    {
        $em = $this->getDoctrine()->getManager();
        $chantiers = $em->getRepository('AppBundle:Chantier')
            ->findAll();
        return $this->render('chantier/allChantier.html.twig', array(
           'chantiers'=> $chantiers,
        ));
    }

    /**
     * Creates a new chantier entity.
     *
     * @Route("/new", name="chantier_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $chantier = new Chantier();

        $form = $this->createForm('AppBundle\Form\ChantierType', $chantier);
        $form->handleRequest($request);

        $user =$this->container->get('security.token_storage')->getToken()->getUser();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $chantier->setUser($user);
            $chantier->upload();
            $em->persist($chantier);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Le chantier '.$chantier->getName().' a bien été creer !! ')
            ;
            $this->dispatch(NotificationEvent::NEW_CHANTIER,new ChantierEvent($chantier));

            return $this->redirectToRoute('chantier_show', array('id' => $chantier->getId()));
        }

        return $this->render('chantier/new.html.twig', array(
            'chantier' => $chantier,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a chantier entity.
     *
     * @Route("/{id}", name="chantier_show")
     * @Method("GET")
     */
    public function showAction(Chantier $chantier)
    {
        $deleteForm = $this->createDeleteForm($chantier);
        $acomptes = $this->getAcompteForChantier($chantier);

        $em = $this->getDoctrine()->getManager();

        return $this->render('chantier/show.html.twig', array(
            'id' => $chantier->getId(),
            'chantier' => $chantier,
            'acomptes'=>$acomptes,
            'delete_form' => $deleteForm->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing chantier entity.
     *
     * @Route("/{id}/edit", name="chantier_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Chantier $chantier)
    {
        $editForm = $this->createForm(ChantierType::class, $chantier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $chantier->upload();
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('chantier_edit', array('id' => $chantier->getId()));
        }

        return $this->render('chantier/edit.html.twig', array(
            'id' => $chantier->getId(),
            'name' => $chantier->getName(),
            'chantier' => $chantier,
            'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a chantier entity.
     *
     * @Route("/{id}", name="chantier_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Chantier $chantier)
    {
        $form = $this->createDeleteForm($chantier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($chantier);
            $em->flush();
        }

        return $this->redirectToRoute('chantier_index');
    }

    public function getAcompteForChantier($id)
    {
        $em = $this->getDoctrine()->getManager();

        $acomptes = $em->getRepository(Acompte::class)
            ->createQueryBuilder('acompte')
            ->select('acompte')
            ->addSelect('chantier')
            ->leftJoin('acompte.chantier', 'chantier')
            ->where('chantier = :chantier_id')
            ->setParameter('chantier_id', $id)
            ->getQuery()
            ->execute();

        //dump($acomptes);die();
        return $acomptes;
    }

    /**
     * Creates a form to delete a chantier entity.
     *
     * @param Chantier $chantier The chantier entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Chantier $chantier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chantier_delete', array('id' => $chantier->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }


    private function dispatch($eventName, Event $event)
    {
        return $this->get('event_dispatcher')->dispatch($eventName,$event);
    }
}
