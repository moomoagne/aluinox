<?php
/**
 * Created by PhpStorm.
 * User: medoune
 * Date: 28/12/2016
 * Time: 11:58
 */

namespace AppBundle\Listener;


use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{

    protected $templating;
    protected $kernel;
    protected $logger;

    public function __construct(EngineInterface $templating, $kernel, Logger $logger)
    {
        $this->templating = $templating;
        $this->kernel = $kernel;
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if('prod' == $this->kernel->getEnvironment()){
            $exception = $event->getException();

            $response = new Response();
            $response->setContent(
              $this->templating->render(
                  ':error:error.html.twig',
                  array('exception' => $exception)
              )
            );

            if($exception instanceof HttpExceptionInterface){
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            }else{
                $response->setStatusCode(500);
                $this->logger->addCritical($exception->getMessage());
            }

            $event->setResponse($response);
        }
    }





}