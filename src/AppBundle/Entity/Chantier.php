<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Chantier
 *
 * @ORM\Table(name="chantier")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ChantierRepository")
 * @ORM\HasLifecycleCallbacks()
 */


class Chantier
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @return string
     */
    public function getImage ()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage ($image)
    {
        $this->image = $image;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="budget", type="decimal", precision=16)
     */
    private $budget;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255)
     */
    private $lieu;

    /**
     * @var active
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;


    /**
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * @param string $lieu
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;
    }


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="datetime")
     */
    private $dateFin;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="chantiers")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;


    private $images_files;

    /**
     * @return mixed
     */
    public function getImagesFiles ()
    {
        return $this->images_files;
    }

    /**
     * @param mixed $images_files
     */
    public function setImagesFiles (UploadedFile $images_files)
    {
        $this->images_files = $images_files;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Chantier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Chantier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Chantier
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Chantier
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**@param ExecutionContextInterface $context

    public function validate(ExecutionContextInterface $context)
    {
        $dd = $this->getDateDebut();
        $df = $this->getDateFin();

        if ($df->format("dd/mm/yy") < $dd->format("dd/mm/yy")) {
            $context->addViolationAt(
                'dateDebut',
                'Erreur! la date de debut est supérieure à la date de final',
                array(),
                null
            );
        }
    }
     * */

    public function __construct()
    {
        $this->dateDebut = new \DateTime();
        $this->active = true;
    }


    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Chantier
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set budget
     *
     * @param string $budget
     *
     * @return Chantier
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return string
     */
    public function getBudget()
    {
        return $this->budget;
    }

    public function getUploadDir ()
    {
        return 'telecharger/images';
    }

    public function getAbsolutRoot ()
    {
        return $this->getUploadRoot().$this->image;
    }

    public function getWebPath ()
    {
        return $this->getUploadDir().'/'.$this->image;
    }

    public function getUploadRoot ()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir().'/';
    }

    public function upload ()
    {
        if ($this->images_files === null){return;}
        $this->image = $this->images_files->getClientOriginalName();
        //var_dump($this->getUploadRoot());die();
        if (!is_dir($this->getUploadRoot())){
            mkdir($this->getUploadRoot(), '0777', true);
        }
        $this->images_files->move($this->getUploadRoot(),$this->image);
        unset($this->images_files);
    }

    public function __toString ()
    {
        return (string) $this->name;
    }

}
