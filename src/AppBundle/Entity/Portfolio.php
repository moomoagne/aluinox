<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Portfolio
 *
 * @ORM\Table(name="portfolio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PortfolioRepository")
 */
class Portfolio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var String
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @return String
     */
    public function getDescription ()
    {
        return $this->description;
    }

    /**
     * @param String $description
     */
    public function setDescription ($description)
    {
        $this->description = $description;
    }



    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    private $images_files;

    /**
     * @return mixed
     */
    public function getImagesFiles ()
    {
        return $this->images_files;
    }

    /**
     * @param mixed $images_files
     */
    public function setImagesFiles ($images_files)
    {
        $this->images_files = $images_files;
    }


    /**
     * @return string
     */
    public function getImage ()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage ($image)
    {
        $this->image = $image;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Portfolio
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getUploadDir ()
    {
        return 'portfolio/images';
    }

    public function getAbsolutRoot ()
    {
        return $this->getUploadRoot().$this->image;
    }

    public function getWebPath ()
    {
        return $this->getUploadDir().'/'.$this->image;
    }

    public function getUploadRoot ()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir().'/';
    }

    public function upload ()
    {
        if ($this->images_files === null){return;}
        $this->image = $this->images_files->getClientOriginalName();
        //var_dump($this->getUploadRoot());die();
        if (!is_dir($this->getUploadRoot())){
            mkdir($this->getUploadRoot(), '0777', true);
        }
        $this->images_files->move($this->getUploadRoot(),$this->image);
        unset($this->images_files);
    }
}
